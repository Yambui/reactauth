import {AUTH_USER, AUTH_ERROR} from "./types";
import axios from 'axios';

export const signup = (formProps, callback) => {
    return async function (dispatch) {
        try {
            const response = await axios.post('http://localhost:3090/signup', formProps);


            dispatch({type: AUTH_USER, payload: response.data.token});
            localStorage.setItem('token', response.data.token)
            callback();
        } catch (e) {
            dispatch({type: AUTH_ERROR, payload: "EMAIL IN USE"})
        }
    }
};

export const signout = () => {
    localStorage.removeItem('token');
    return {
        type: AUTH_USER,
        payload: ''
    };
};

export const signin = (formProps, callback) => {
    return async function (dispatch) {
        try {
            const response = await axios.post('http://localhost:3090/signin', formProps);


            dispatch({type: AUTH_USER, payload: response.data.token});
            localStorage.setItem('token', response.data.token)
            callback();
        } catch (e) {
            dispatch({type: AUTH_ERROR, payload: "Wrong password"})
        }
    }
};